# shinobi-tensorflow.js

[![pipeline status](https://gitlab.com/cctv_web_cam/shinobi/objectdetection_mqtt_plugin/badges/master/pipeline.svg)](https://gitlab.com/cctv_web_cam/shinobi/objectdetection_mqtt_plugin/-/commits/master)
[![coverage report](https://gitlab.com/cctv_web_cam/shinobi/objectdetection_mqtt_plugin/badges/master/coverage.svg)](https://gitlab.com/cctv_web_cam/shinobi/objectdetection_mqtt_plugin/-/commits/master)

This is a [Shinobi](https://gitlab.com/Shinobi-Systems/Shinobi) plugin, 
based on the preinstalled Tensorflow plugin. It has being expanded to 
post an image and associated prediction matrices for objects detected 
by Tensorflow. These are posted to a message queue for further 
processing.

A docker environment for development is offered, containing:
- MQTT
- Mysql DB
- Reverse proxy (Nginx)
- Shinobi installation containing this plugin

Please refer to the *README.md* file within the *docker* subfolder
for further details.


## Architecture

The plugin forks a NodeJS process (notify.js) that receives object 
detections from the parent process (shinobi-tensorflow.js) over a
NodeJS IPC (Inter Process Channel). These are forwarded to listening
external systems, notified via a publish subscribe mechanism using
an MQTT message queue broker.

![PluginArchitecture](https://i.ibb.co/NC32hZn/mqtt-tensorflow-plugin-architecture.png)

An object detection is dispatched to the MQTT broker when the following 
conditions are met:

- Confidence is >= minimum confidence config parameter, _minConfidence_.
- The predicted object class matches at least one of those included 
  in a configuration parameter, _objectClasses_.
- Timeout period is not running. A timeout period is triggered after
  a message has been despatched onto the queue. If the timer is
  running then object detections are skipped, i.e. not posted on
  queue.

Messages containing object detections are published onto the MQTT broker
in the following JSON format:

``` json
{
  "group": "myGroup",
  "monitorId": "myMonitorId",
  "time": "2020-04-24T13:49:16+00:00",
  "plug": "TensorFlow-WithFiltering-And-MQTT",
  "details": {
    "plug": "TensorFlow-WithFiltering-And-MQTT",
    "name": "Tensorflow",
    "reason": "object",
    "matrices": [{
      "x": "2.313079833984375",
      "y": "1.0182666778564453",
      "width": "373.25050354003906",
      "height": "476.9341278076172",
      "tag": "person",
      "confidence": "0.7375929355621338"
    }],
    "img": "base64",
    "imgHeight": "64",
    "imgWidth": "48",
    "time": "2020-04-24T13:49:16+00:00"
  }
}
```

Each detection is published to MQTT with the following topic format to allow 
notifications to be sent for each camera in
[Shinobi](https://gitlab.com/Shinobi-Systems/Shinobi):

``` js
`shinobi/${group}/${monitor}/trigger`
```


## Development Technologies

The following technologies are used:

- **[Docker](https://www.docker.com/)** and [Docker-Compose](https://docs.docker.com/compose/): For providing a configurable and 
quick way to run an MQTT broker for end-to-end testing.
- **[Jest](https://jestjs.io/)**: For writing and running tests.
- **[MQTT.js](https://github.com/mqttjs)**: Message queue for publishing and subscribing object detections.
- **[NodeJS](https://nodejs.org/en/)**: For implementing the [Shinobi](https://gitlab.com/Shinobi-Systems/Shinobi) plugin.
- **[Pino](https://github.com/pinojs/pino)**: Fast JSON logging framework.
- **[GitLab CI](https://docs.gitlab.com/ee/ci/quick_start/)**: For defining Continuous Integration and Delivery pipelines to
facilitate dependability and release management.
- **[Pre-Commit](https://pre-commit.com)**: The author developed a pre-commit hook that can be used to validate a CI pipeline,
specified in a *.gitlab-ci.yml* file. This repository uses the pre-commit hook, configured within the *.pre-commit-config.yaml*
file. Consequently, commits are declined for invalid *gitlab-ci.yml* content.


## Usage of GitLab CI

A docker environment has been provided for use on Continuous Integration servers.
This is provided by the following docker-compose files:

- docker/docker-compose.yml
- docker/docker-compose.ci.yml

A Continuous Integration build is configured in *.gitlab-ci.yml* within the root
of the repository. This defines stages for building the plugin and running
tests on the GitLab Continuous Integration server when changes are pushed to 
a repository branch. 

While developing the CI pipeline a syntax error would occasionally leak
into the file and be commited into the master branch. This became tiresome and cumbersome,
breaking the build. Initially, the author investigated using a git *pre-receive* hook on the
server. However, sadly, this is not offered on the *gitlab.com* free plan. Consequently,
the author developed a Ruby CLI gem that dispatches escaped JSON content for the *.gitlab-ci.yml* file
to the remote GitLab API. This then summarises the response. The gem is available
[here](https://rubygems.org/gems/gitlab-lint-client) and can be used from within a pre-commit hook.
The source code is available from [here](https://github.com/dcs3spp/validate-gitlab-ci) and offers a
[pre-commit](https://pre-commit.com) hook, *gitlab-validation-ci*. This hook is used by this repository in 
the file *.pre-commit-config.yaml*. Now, if a commit is made for a .gitlab-ci.yml file that 
contains a syntax error, the commit is declined.

The CI pipeline also specifies a release job that currently packs a source
tarball of the plugin node package as a release artefact. Semantic versioning is used and derived from the
git tag. This job is triggered for branches named release-v*. Furthermore, the release job publishes 
an npm package to a private registery served at *bytesafe.dev*. This provides semi-automated release
delivery.


## Installation

Download and install the plugin into the Shinobi plugins folder.

```
cd <sinobi folder>/plugins
git clone https://gitlab.com/cctv_web_cam/shinobi/objectdetection_mqtt_plugin.git objectdetection
```

1. Go to the Shinobi installation directory. ```cd <my shinobi>/plugins/objectdetection```
2. Copy the sample config file. ```cp config.sample.json config.json```
3. Run the installation script _INSTALL.sh_. ```./INSTALL.sh```

The plugin installer script will prompt for installation of the 
tensorflow gpu library to offload detection processing to the graphics 
card. Type _y_ to install this feature.

The plugin also prompts if the CPU is Intel Quad Core 2. This will install
tensorflow nodejs precompiled library compatible with CPU Intel Quad Core 2
processors. Type _y_ to install this feature. 

4. Start the plugin ```sudo pm2 start shinobi-tensorflow.js```

If the plugin has been successfully started then _TensorFlow-WithFiltering-And-MQTT_ 
should be revealed in the header of the Object Detection configuration section of the
Shinobi web interface for the camera monitor.



## Configuration

A sample configuration file is listed below:

```
{
  "host":"localhost",
  "hostPort":8082,
  "key":"key generated by installer script INSTALLER.sh",
  "mode":"client",
  "mqtt": {
    "cafile":"full path to CA file",
    "host": "localhost",
    "password": "pass",
    "rejectUnauthorised": "false",
    "user": "user"
  },
  "objectDetection": {
    "minConfidence":0.70,
    "objectClasses":"person",
    "timeoutPeriod":1200000
  },
  "plug":"TensorFlow-WithFiltering-And-MQTT",
  "port":8080,
  "tfjsBuild":"cpu",
  "type":"detector"
}
```

 cafile: `${__dirname}/../../docker/certs/localCA.crt`,
      

The plugin is configured by the _mqtt_ and _objectDetection_ sections.


### Message Queue Configuration

[MQTT.js](https://github.com/mqttjs) is used as the message broker. This can 
be configured using the following options in the JSON plugin configuration file:


| Option              | Description                                   | Example                           |
|---------------------|-----------------------------------------------|-----------------------------------|
| mqttsUrl            | Url for mqtts including user credentials      | mqtts://user:pass@localhost:8883  |
| cafile              | Path to certificate authority file            | /some/path/to/cafile              |
| host                | Hostname of MQTT broker. MQTT_HOST            | localhost | MQTT_HOST=localhost   |
|                     | environment variable if set, takes precedence |                                   |
| password            | Optional, password. If omitted try from       | pass | MQTT_PASSWORD=pass         |
|                     | environment variable MQTT_PASSWORD. Env vars  |                                   |
|                     | take precedence if set                        |                                   |
| rejectUnauthorised  | Should untrusted CA certificates be rejected? | true                              | 
| user                | Optional, username. If omitted, then read     | user | MQTT_USER=user             |
|                     | from MQTT_USER environment variable. Env vars |                                   |
|                     | take precedence if set                        |                                   |

The environment variables: MQTT_HOST, MQTT_PASSWORD and MQTT_USER take precedence over the host, user
and password configuration properties. These are required when running the plugin within a docker
container.


### Object Detection Configuration


| Option              | Description                                   | Example                           |
|---------------------|-----------------------------------------------|-----------------------------------|
| minConfidence       | decimal number between 0 and 1 to describe    | 0.70                              |
|                     | minimum confidence threshold for a message to |                                   |
|---------------------|-----------------------------------------------|-----------------------------------|
| objectClasses       | Comma separated list of objects to trigger    | person, cat                       |
|                     | messages for                                  |                                   |
|---------------------|-----------------------------------------------|-----------------------------------|
| timeoutPeriod       | Timeout, in seconds, that runs after a        | 120000                            |
|                     | detection has been sent to message queue.     |                                   |
|                     | If a detection is received and the            |                                   |
|                     | timer is running then it is not sent onto the |                                   |
|                     | queue. This was done so that if a recording   |                                   |
|                     | is triggered by motion then a obhect image    |                                   |
|                     | image can be sent every _n_ seconds           |                                   |
|---------------------|-----------------------------------------------|-----------------------------------|


## Running Tests

[Jest](https://jestjs.io/) has been used to write and run tests. The following scripts can be run from
within the _plugins/objectdetection_ folder:

``` bash
# run all tests
yarn run test

# run unit tests
yarn run test:unit

# run end to end tests
yarn run test:e2e
```

Coverage reports are output to _<rootDir>/tests/coverage_. This is not committed
into the repository.

The end-to-end tests require docker and docker-compose to be installed on the developers machine. 
Docker is used to quickly configure and start an MQTT broker on the developer's environment. Developers
must setup server certificates in _./tests/docker/certs_ folder. Further details on configuring the
docker MQTT broker can be found in _tests/docker/README.md_.

## Test Coverage

Test coverage is available for the *master* branch at [https://cctv_web_cam.gitlab.io/shinobi/objectdetection_mqtt_plugin/coverage/html/index.html](https://cctv_web_cam.gitlab.io/shinobi/objectdetection_mqtt_plugin/coverage/html/index.html)

## Linting

The following scripts have been provided with respect to linting the plugin source code:

``` bash
# display linting warning and errors 
yarn run lint

# display and try to automatically fix linting warnings and errors
yarn run lint:fix
```

The Continuous Integration Pipeline also performs linting of source code. Source code linting is based upon the 
[Javascript Standard Style](https://github.com/standard/standard#why-should-i-use-javascript-standard-style)

Furthermore, the *gitlab-ci.yaml* file is automatically linted each time it is staged for commit. This is achieved via a pre-commit hook.
