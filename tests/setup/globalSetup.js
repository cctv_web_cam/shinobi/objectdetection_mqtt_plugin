const certFileStatus = require('./utils')

/**
 * Throw an error if one of the following files are missing from
 * <root dir>/tests/docker/certs:
 * - localCA.crt
 * - server.crt
 * - server.key
 */
module.exports = async () => {
  const info = await certFileStatus()

  if (!info.caExists || !info.serverCertExists || !info.serverKeyExists) {
    throw new Error('3 certificates should exist in <root dir>/tests/docker/certs :: localCA.crt, server.crt and server.key')
  }
}
