const NodeEnvironment = require('jest-environment-node')
const { getMqttConfig, loadJSON } = require('../../utils/index')

/**
 * @class TestEnvironment
 * Exposes a config property on global that contains
 * configuration properties as follows:
 * {
 *   host: <string>,
 *   mqttUrl: `mqtt://${user}:${password}@${host}:1883`,
 *   mqttsUrl: `mqtts://${user}:${password}@${host}:8883`,
 *   password: <string>,
 *   user: <string>
 * }
 *
 * These are loaded from config file ../config/conf.json
 * Environment variables take precedence over the config
 * file settings
 */
class TestEnvironment extends NodeEnvironment {
  /**
   * Create mqtt connection url from config file or environment
   * variables. Environment variables take precedence over
   * config file properties. The environment variables are:
   * MQTT_HOST MQTT_PASSWORD MQTT_PORT MQTT_USER
   * @returns {Object} Config object placed as property on
   * global, e.g. { config: { mqttUrl: string } }
   */
  async setup () {
    const config = await loadJSON(`${__dirname}/../config/conf.json`)
    this.global.config = getMqttConfig(config)
  }

  async teardown () {
    await super.teardown()
  }

  runScript (script) {
    return super.runScript(script)
  }
}

module.exports = TestEnvironment
