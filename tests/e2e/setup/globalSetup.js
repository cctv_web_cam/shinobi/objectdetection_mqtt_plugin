const { loadJSON, validateEnvironment } = require('../../utils')

/**
 * Throw an error under one of the following conditions:
 * - ../config/conf.json does not exist
 * - mqtt.host, mqtt.cafile or mqtt.rejectUnauthorised are missing from
 * - MQTT_HOST, MQTT_PASSWORD or MQTT_USER are missing AND the user or
 *   password property is missing from the config file
 */
module.exports = async () => {
  // load the config file
  const CONF_FILE = `${__dirname}/../config/conf.json`
  const config = await loadJSON(CONF_FILE)

  // throw an error if mandatory properties missing from test config file
  if (config && config.mqtt && (!config.mqtt.host | !config.mqtt.cafile | !config.mqtt.rejectUnauthorised)) {
    throw new Error('Please ensure test config file contains mqtt.host, mqtt.cafile and mqtt.rejectUnauthorised properties')
  }

  // throw an error if environment variables not set and mqtt.user and mqtt.password are missing from config file
  const env = validateEnvironment()
  if (!env && config && config.mqtt && (!config.mqtt.user || !config.mqtt.password)) {
    throw new Error('Please ensure test config file contains mqtt.user and mqtt.password properties or ensure MQTT_HOST, MQTT_PASSWORD and MQTT_USER are set')
  }
}
