# Changelog

## [1.0.2] - 2020-07-15
### Added
- Pre-commit hook to allow CI Pipeline yaml files to be automatically linted upon a local commit.

### Changed
- Documentation updated to explain Pipeline process and precommit usage.
- CI Pipeline test job updated to combine test coverage reports.
- CI Pipeline release job updated to publish npm package to a private *bytesafe.dev* registry.
- CI Pipeline release job updated to set package version from git tag, package.json version is not touched.

## [1.0.1] - 2020-06-26
### Added
- GitLab CI Pipeline, see .gitlab-ci.yml
- Added 'Usage of GitLab CI' section to repository readme.
- *jest-junit* test reports added to jest config files

### Changed
- Makefile pruned to include rules for building development docker-compose only. CI make rules removed, equivalent tasks scripted in CI jobs.
- Image for docker-compose.ci.yml plugin service updated to $CI_REGISTRY_IMAGE/plugin:$CI_COMMIT_SHA, unique to each build triggered by commit on CI server.

### Removed
- Makefile rules for building and starting CI docker images. Rule *down_ci* remains as a helper to stop docker-compose services, referenced within the test CI job.

## [1.0.0] - 2020-06-22
Initial version for plugin consisting of docker-compose architecture for development and continuous integration environments.
Unit tests and end to end tests provided.
