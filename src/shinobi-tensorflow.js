'use strict'

/**
 * Shinobi - ObjectDetection Tensorflow plugin based on:
 *   Shinobi - Tensorflow Plugin Copyright (C) 2016-2025 Moe Alam, moeiscool
 *
 * Added functionality to send notification to MQTT broker using connection
 * parameters in plugin config. Messages sent on queue must pass the filter
 * conditions defined in plugin config:
 * - >= minimum confidence threshold
 * - predicted object class should match set of classes defined by the filter
 *
 * Future enhancements:
 * Read the filter configuration defined in shinobi's filter detection
 * I am new to the source code, for now using plugin JSON config....
 *
 * Copyright (C) 2020 Simon Pears
 */

const config = require('../conf.json')
const { forkChildProcess } = require('./libs/utils/src')

/* =============== Constants ============================================== */
const ENCODING = 'base64'
/* =============== End Constants ========================================== */

/* =============== Functions ============================================== */
/**
 * Initialise the plugin base
 * @param {Object} config - Object representation of ./conf.json
 * @throws {Error} - When fails to retry load pluginBase.js
 * @returns {Object} - Plugin base object, refer to Moe Alam's source code
 * for pluginBase.js at <shinobi_home>/plugins/
 */
function initPluginBase (config) {
  let s
  try {
    s = require('../../pluginBase.js')(__dirname, config)
  } catch (err) {
    console.error(`Error encountered loading ../../pluginBase.js => ${err}`)
    s = require('../pluginBase.js')(__dirname, config)
  }

  return s
}

/**
 * Forward signal to child process (notify.js) and exit with code 128+exitCode
 * @param {string} signal - Process signal received, e.g. SIGINT, SIGTERM
 * @param {number} exitCode - Exit code equivalent for signal
 * @param {Process} child - Child process, there is only one
 */
function dispose (signal, exitCode, child) {
  try {
    console.log(`Killing child process [notify.js], [PID ${child.pid}] with signal [${signal}]`)
    child.kill(signal)
  } catch (error) {
    console.error(`Error raised while killing child process [notify.js] with signal ${signal} :: error message = ${error.message}`)
  } finally {
    console.log(`[shinobi-tensorflow.js] exiting with code [${128 + exitCode}]`)
    process.exit(128 + exitCode)
  }
}
/* =============== End Functions ============================================== */

/* =============== Main ======================================================= */
try {
  const s = initPluginBase(config)

  const ObjectDetectors = require('./ObjectDetectors.js')(config)
  const filterObj = require('./filter.js')(config.objectDetection.minConfidence, config.objectDetection.objectClasses)
  s.systemLog('TensorFlow-WithFiltering-And-MQTT acquired filter object...')

  const childProcessPromise = forkChildProcess(config, `${__dirname}/notify.js`)

  /** Wait for the child process to have initialised */
  childProcessPromise.then(childProcess => {
    s.systemLog(`TensorFlow-WithFiltering-And-MQTT forked child process [notify.js] with [PID ${childProcess.pid}] for sending detections to MQTT broker`)

    /**
     * Signal handlers
     * Forward SIGTERM and SIGINT to child process
     */
    process.on('SIGTERM', (signal, code) => {
      dispose(signal, code, childProcess)
    })

    process.on('SIGINT', (signal, code) => {
      dispose(signal, code, childProcess)
    })

    /**
     * Detect object listener
     */
    s.detectObject = (buffer, d, tx, frameLocation) => {
      new ObjectDetectors(buffer).process().then((resp) => {
        let filtered = false

        var results = resp.data

        if (results[0]) {
          var mats = []
          results.forEach(function (v) {
            mats.push({
              x: v.bbox[0],
              y: v.bbox[1],
              width: v.bbox[2],
              height: v.bbox[3],
              tag: v.class,
              confidence: v.score
            })

            if (!filtered) {
              filtered = filterObj.filter(v.class, v.score)
            }
          })

          var isObjectDetectionSeparate = d.mon.detector_pam === '1' && d.mon.detector_use_detect_object === '1'
          var width = parseFloat(isObjectDetectionSeparate && d.mon.detector_scale_y_object ? d.mon.detector_scale_y_object : d.mon.detector_scale_y)
          var height = parseFloat(isObjectDetectionSeparate && d.mon.detector_scale_x_object ? d.mon.detector_scale_x_object : d.mon.detector_scale_x)

          tx({
            f: 'trigger',
            id: d.id,
            ke: d.ke,
            details: {
              plug: config.plug,
              name: 'Tensorflow',
              reason: 'object',
              matrices: mats,
              imgHeight: width,
              imgWidth: height,
              time: resp.time
            }
          })

          // if the detection passed the filter rules then dispatch onto the message queue
          if (filtered) {
            const message = {
              group: d.ke,
              time: resp.time,
              monitorId: d.id,
              plug: config.plug,
              details: {
                plug: config.plug,
                name: 'Tensorflow',
                reason: 'object',
                matrices: mats,
                img: buffer.toString(ENCODING),
                imgHeight: width,
                imgWidth: height,
                time: resp.time
              }
            }

            try {
              childProcess.send(message)
            } catch (err) {
              console.error(`${err}`)
            }
          }
        }
      })
    }
  })
    .catch((reason) => {
      console.error(`Child process [notify.js] failed to start due to invalid arguments => ${reason}`)
    })
} catch (error) {
  console.error(`Failed retry for loading base plugin => ${error}`)
}
/* =============== End Main ======================================================= */
