#!/usr/bin/with-contenv bash
# ==============================================================================
# Creates initial Shinobi configuration in case it is non-existing
# ==============================================================================
# shellcheck disable=SC1091
source /usr/lib/bashio/bashio.sh

bashio::log.info "Checking if need to perform any first time initialisation of configuration files..."

declare CONFIG

if ! bashio::fs.directory_exists '/share/shinobi'; then
    mkdir -p '/share/shinobi'
fi

# write default /data/conf.json from /opt/shinobi/conf.sample.json if not present
if ! bashio::fs.file_exists '/data/conf.json'; then
    CONFIG=$(</opt/shinobi/conf.sample.json)

    CONFIG=$(bashio::jq "${CONFIG}" ".cpuUsageMarker=\"cpu\"")
    CONFIG=$(bashio::jq "${CONFIG}" ".port=80")
    CONFIG=$(bashio::jq "${CONFIG}" ".videosDir=\"/share/shinobi/\"")

    echo "${CONFIG}" > /data/conf.json
fi

if ! bashio::fs.file_exists '/data/super.json'; then
    cp /opt/shinobi/super.sample.json /data/super.json
fi

if ! bashio::fs.file_exists '/data/pm2Shinobi.yml'; then
    cat << EOF > /data/pm2Shinobi.yml
apps:
  - script: '/opt/shinobi/camera.js'
    name: 'Camera-App'
    kill_timeout: 5000
  - script: '/opt/shinobi/cron.js'
    name: 'Cron-App'
    kill_timeout: 5000
  - script: '/opt/shinobi/plugins/objectdetection/src/shinobi-tensorflow.js'
    name: 'Objectdetection-App'
    kill_timeout: 5000
EOF
fi
