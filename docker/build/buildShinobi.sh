#!/usr/bin/with-contenv bash
# ==============================================================================
# This file clones shinobi and builds package dependencies
# ==============================================================================
# shellcheck disable=SC1091
source /usr/lib/bashio/bashio.sh

bashio::log.info 'Building shinobi...'

#-----------------------------------------------------------------------
# Make a build directory
#-----------------------------------------------------------------------
bashio::log.info 'Creating build directory...'
mkdir /build/shinobi

# ----------------------------------------------------------------------
# Download Shinobi source 
# ----------------------------------------------------------------------
bashio::log.info 'Cloning Shinobi source...'
git clone -b master --single-branch \
    https://gitlab.com/Shinobi-Systems/Shinobi.git /build/shinobi/

# ----------------------------------------------------------------------
# Download and build package dependencies
# ----------------------------------------------------------------------
bashio::log.info 'Building package dependencies...'
cd /build/shinobi
yarn add mysql
yarn install --audit --production
